import css from './style.css';
import { Root } from "./components.jsx";
import React from "react";
import ReactDOM from "react-dom";

fetch('http://textual.lxc:8000/components/products/root')
    .then(response => response.json())
    .then(render)

function render(props) {
    console.log(props);
    ReactDOM.render(
        React.createElement(Root, props),
        document.getElementById('root')
    );
}
