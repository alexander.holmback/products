import React, { useState } from "react";

export function Root(props) {
    return (
        <>
        <Navbar {...props.navbar} />
        <ProductTable {...props.product_table} />
        </>
    );
}

export function Navbar(props) {
    const [open, setOpen] = useState(false);

    return (
        <div className="navbar">
            <a href={props.home}>
                <img src="logo.png" alt="Logo" />
            </a>
            <i className="menu-button material-icons" onMouseEnter={() => setOpen(true)}>menu</i>
            <div className="menu-content" style={{display: open ? 'block' : 'none' }} onMouseLeave={() => setOpen(false)}>
                <ul>
                    {props.items.map((item, key) => 
                        item ?
                        <li key={key}><a href={item.href} className="menu-item">{item.label}</a></li>
                        : <li key={key} className="separator"></li>
                    )}
                </ul>
            </div>
        </div>
    );
}

export function ProductTable(props) {
    let columns = f => Object.entries(props.columns).map(f)
    return (
        <div className="product-table">
            <table>
                <thead>
                    <tr>{columns(([k, v], i) => <th key={k}>{v}</th>)}</tr>
                </thead>
                <tbody>
                    {props.products.map(product => 
                        <tr key={product.id}>
                            {columns(([k, v], i) => <td key={k}>{product[k]}</td>)}
                        </tr>
                    )}
                </tbody>
            </table>
        </div>
    )
}
